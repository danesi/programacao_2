/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package testeheranca;

/**
 *
 * @author boemo
 */
public class Veiculo {
   private String motor;
   private String combustivel;
   private String marca;
   private String anoFabricacao;
   protected double velocidade;

   public void acelerar(){
   this.velocidade++;
   }
   public void desacelerar(){
    this.velocidade--;  
   }
    public double getVelocidade() {
        return velocidade;
    }

    public void setVelocidade(double velocidade) {
        this.velocidade = velocidade;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getCombustivel() {
        return combustivel;
    }

    public void setCombustivel(String combustivel) {
        this.combustivel = combustivel;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getAnoFabricacao() {
        return anoFabricacao;
    }

    public void setAnoFabricacao(String anoFabricacao) {
        this.anoFabricacao = anoFabricacao;
    }

    public Veiculo(String motor, String combustivel, String marca, String anoFabricacao) {
        this.motor = motor;
        this.combustivel = combustivel;
        this.marca = marca;
        this.anoFabricacao = anoFabricacao;
    }

    public Veiculo(String motor) {
        this.motor = motor;
    }
   
}
