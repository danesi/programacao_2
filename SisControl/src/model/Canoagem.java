/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author boemo
 */
public class Canoagem extends Prova{
    
    private String tipo;//sprint ou maratona
    private String modalidade;// numero de remadores 1, 2 , 4, 8
    private String tipoEquipamento;// canoa, caiaque, canoa havaiana

    public Canoagem(double distancia, double tempo, String data, String local) {
        super(distancia, tempo, data, local);
    }

    public String getTipoEquipamento() {
        return tipoEquipamento;
    }

    public void setTipoEquipamento(String tipoEquipamento) {
        this.tipoEquipamento = tipoEquipamento;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }
}
