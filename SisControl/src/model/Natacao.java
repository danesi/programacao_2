/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author boemo
 */
public class Natacao extends Prova {
    String modalidade;

    public Natacao(double distancia, double tempo, String data, String local) {
        super(distancia, tempo, data, local);
    }

    public String getModalidade() {
        return modalidade;
    }

    public void setModalidade(String modalidade) {
        this.modalidade = modalidade;
    }
    
}
