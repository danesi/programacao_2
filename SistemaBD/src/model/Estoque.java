/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author danesi
 */
public class Estoque {
    private String codigo;
    private String placa;
    private String codGaragem;

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getCodGaragem() {
        return codGaragem;
    }

    public void setCodGaragem(String codGaragem) {
        this.codGaragem = codGaragem;
    }
    
}
