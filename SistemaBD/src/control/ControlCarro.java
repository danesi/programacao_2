
package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import model.Carro;

public class ControlCarro {
    
    Connection con;

    public ControlCarro(Connection con) {
        this.con = con;
    }
    
    public void inserirCarro(Carro carro){
    String SQL="insert into carro values(?,?,?)";
    try{
        
        PreparedStatement ps=getCon().prepareStatement(SQL);
        ps.setString(1,carro.getPlaca());
        ps.setString(2, carro.getModelo());
        ps.setString(3,carro.getCor());
        ps.executeUpdate();
        
    }catch(Exception ex){
        System.out.println("Erro : "+ex.getMessage());
    
    }
    }
    
    
    public void alterarCarro(Carro carro){
    String SQL="update carro set modelo=?,cor=? where placa=?";
   try{
        PreparedStatement ps=getCon().prepareStatement(SQL);
        ps.setString(1,carro.getModelo());
        ps.setString(2,carro.getCor());
        ps.setString(3,carro.getPlaca());
        ps.executeUpdate();
    }catch(Exception ex){
        System.out.println("Erro : "+ex.getMessage());
    }
    }
    
    public void excluirCarro(String placa){
    String SQL="delete from carro where placa = ?";
    try{
        
        PreparedStatement ps=getCon().prepareStatement(SQL);
        ps.setString(1,placa);
        ps.executeUpdate();
        
    }catch(Exception ex){
        System.out.println("Erro : "+ex.getMessage());
    
    }
    }
    
    public void listarCarro(){
    String SQL="Select * from carro";
    try{
        PreparedStatement ps=getCon().prepareStatement(SQL); 
        
        
        ResultSet rs = ps.executeQuery();
        if(rs!=null){
        while(rs.next()){
        Carro car=new Carro();
        car.setPlaca(rs.getString(1));
            System.out.println(" carro : "+car.getPlaca());
            System.out.println(" modelo : "+rs.getString(2));
            System.out.println(" cor : "+ rs.getString(3));
        }
        }
        
        
    }catch(Exception ex){
        
    }
    
    }

    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
    
    
}
