/*
CREATE TABLE estoque
(
  codigo integer NOT NULL,
  placa character varying(8),
  "codGaragem" integer,
  CONSTRAINT estoque_pkey PRIMARY KEY (codigo),
  CONSTRAINT estoque_placa_fkey FOREIGN KEY (placa)
      REFERENCES carro (placa) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
 */
package control;

import java.sql.Connection;
import java.sql.PreparedStatement;
import model.Estoque;

/**
 *
 * @author boemo
 */
public class ControlEstoque {
    Connection con;
    
    public void inserirEstoque(Estoque estoque){
        String SQL = "insert into estoque values(?, ?, ?)";
        try {
            PreparedStatement ps=getCon().prepareStatement(SQL);
            ps.setString(1, estoque.getCodigo());
            ps.setString(2, estoque.getPlaca());
            ps.setString(3, estoque.getCodGaragem());
            System.out.println("Incerido com sucesso!");
        } catch (Exception e) {
            System.out.println("ERRO" + e.getMessage());
        }
    }
    
    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
    
}
