/*
CREATE TABLE carro
(
  placa varchar(8) NOT NULL,
  modelo varchar(50),
  cor varchar(15),
  CONSTRAINT carro_pkey PRIMARY KEY (placa)
) 
 */
package control;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author boemo
 */
public class conexao {
  private Connection con;  

  public Connection abrirConexao() throws ClassNotFoundException{
      Connection conaux = null;
      try {
          Class.forName("org.postgresql.Driver").newInstance();
          String url="jdbc:postgresql://localhost:5433/revenda";
          String usuario="postgres";
          String senha="ciet";
          conaux=DriverManager.getConnection(url, usuario, senha);
          System.out.println("Conexão realizada com sucesso!!"); 
      } catch (Exception e){
          System.out.println("Ocorreu erro :"+e.getMessage()); 
      }
      return conaux;
  }
    public Connection getCon() {
        return con;
    }

    public void setCon(Connection con) {
        this.con = con;
    }
  
  
  
    
}
