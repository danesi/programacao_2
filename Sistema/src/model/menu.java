/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import control.controlCliente;
import control.controlOs;
import control.controlServico;
import java.io.IOException;
import java.util.Scanner;
import view.telaPrincipal;

/**
 * 
 * @author ANESI
 */

public class menu {
    controlServico contS=new controlServico();
    controlOs contOs = new controlOs();
    controlCliente contCli = new controlCliente();
    servico serv=new servico();
    os Os = new os();
    public void menu01() throws IOException{
        telaPrincipal tela = new telaPrincipal();
        Scanner entrada = new Scanner(System.in);
        System.out.println("|------ MENU -------|");
        System.out.println("|    1. Incerir     |");
        System.out.println("|    2. Apagar      |");
        System.out.println("|    3. Alterar     |");
        System.out.println("|    4. Listar      |");
        System.out.println("|    5. Sair        |");
        System.out.println("|-------------------|");
        System.out.print("Opção: ");
        switch(entrada.nextInt()){
            case 1:
                this.incerir(menu02());
                break;
            case 2:
                this.apagar(menu02());
                break;
            case 3:
                this.alterar(menu02());
                break;
            case 4:
                this.listar(menu02());
                break;
            case 5:
                tela.sair();
                break;    
        }
    }
    
    public int menu02(){
        Scanner entrada = new Scanner(System.in);
        System.out.println("|------ MENU -------|");
        System.out.println("|    1. Serviço     |");
        System.out.println("|    2. Os          |");
        System.out.println("|    3. Cliente     |");
        System.out.println("|-------------------|");
        System.out.print("Opção: ");
        return entrada.nextInt();
    }    
    public void incerir(int opcao) throws IOException{
       
        if(opcao == 1){
            contS.insere();
        }
        if(opcao == 2){
            contOs.insere();
        }
        if(opcao == 3){
            contCli.insere();
        }
    }
    public void apagar(int opcao) throws IOException {
        if(opcao == 1){
            contS.apagar();
        }
        if(opcao == 2){
            contOs.apagar();
        }
        if(opcao == 3){
            contCli.apagar();
        }
    }
    public void alterar(int opcao) throws IOException{ 
       
        if(opcao == 1){
            contS.alterar();
        }
        if(opcao == 2){
            contOs.alterar();
        }
        if(opcao == 3){
            contCli.alterar();
        }
    }
    public void listar(int opcao) {
        
        if(opcao == 1){
            contS.listar();
        }
        if(opcao == 2){
            contOs.listar();
        }
        if(opcao == 3){
            contCli.listar();
        }
    }
}
