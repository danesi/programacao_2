/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import model.servico;

/**
 *
 * @author danesi
 */
public class controlServico {
    servico serv = new servico();
    String[][] bancoServico = new String [5][3];
    public void iniciarBanco(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 3; j++) {
                bancoServico[i][j]="";
            }
        }
        System.out.println("Banco de servico Iniciado!");
    }
    public int verificaArray(){
        int flag = 0;
        for (int i = 0; i < 5; i++) {
            if (bancoServico[i][0].equals("")) {
                flag++;
            }
        }
        flag=flag-5;
        if (flag<0)flag=flag*-1;
            return flag;
      
    }
    
    public void insere() throws IOException{
        Scanner entrada = new Scanner(System.in);
        BufferedReader leitura = new BufferedReader(new InputStreamReader(System.in));
        int cont = 0;
        int ret = verificaArray();
            System.out.print("Código: ");
            serv.setCodigo(entrada.nextInt());
            System.out.print("Descrição: ");
            serv.setDescricao(leitura.readLine());
            System.out.print("Preço: ");
            serv.setPreco(entrada.nextDouble());
            
        if(ret<5){
                bancoServico[ret][0] = ""+serv.getCodigo();
                bancoServico[ret][1] = serv.getDescricao();
                bancoServico[ret][2] = ""+serv.getPreco();
              cont++;
        }
    }
    public void apagar(){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Digite o código do que deverá apagar: ");
        int cod = entrada.nextInt();
        for (int i = 0; i < 4; i++) {
           if(  bancoServico[i][0].equals(""+cod)){ // FORÇA SER UMA STRING
                bancoServico[i][0] = "";
                bancoServico[i][1] = "";
                bancoServico[i][2] = "";
            } 
        }
    }
    public void alterar() throws IOException{
        Scanner entrada = new Scanner(System.in);
        BufferedReader leitura = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("DIgite o código do que deverá alterar: ");
        int cod = entrada.nextInt();
        for (int i = 0; i < 3; i++) {
            if(bancoServico[i][0].equals(""+cod)){ // FORÇA SER UMA STRING
                System.out.print("Novo código: ");
                bancoServico[i][0] = ""+entrada.nextInt();
                System.out.print("Nova descrição: ");
                bancoServico[i][1] = ""+leitura.readLine();
                System.out.print("Novo preço: ");
                bancoServico[i][2] = ""+entrada.nextInt();
            }
        }
        
    }
    public void listar(){
        for (int i = 0; i < 5; i++) {
                if(!bancoServico[i][0].equals("")){
                System.out.println("\nCódigo: "+bancoServico[i][0]+"\nDescrição: " +bancoServico[i][1]+"\nPreço: "+bancoServico[i][2]);
            }
        }
    }
}
