/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import model.cliente;
import model.os;

/**
 *
 * @author ANESI
 */
public class controlOs {
    os Os = new os();
    String[][] bancoOs = new String [5][2];
    public void iniciarBanco(){
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 2; j++) {
                bancoOs[i][j]="";
            }
        }
        System.out.println("Banco OS iniciado!");
    }
    public int verificaArray(){
        int flag = 0;
        for (int i = 0; i < 5; i++) {
            if (bancoOs[i][0].equals("")) {
                flag++;
            }
        }
        flag=flag-5;
        if (flag<0)flag=flag*-1;
            return flag;
      
    }
    
    public void insere() throws IOException{
        Scanner entrada = new Scanner(System.in);
        BufferedReader leitura = new BufferedReader(new InputStreamReader(System.in));
        int ret=verificaArray();
        System.out.print("Código: ");
            Os.setCodigo(entrada.nextInt());
            System.out.print("Descrição: ");
            Os.setData(leitura.readLine());
        if(ret<5){
        bancoOs[ret][0] = ""+Os.getCodigo();
        bancoOs[ret][1] = ""+Os.getData();
        }
    }
    public void apagar(){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Digite o código do que deverá apagar: ");
        int cod = entrada.nextInt();
        for (int i = 0; i < 4; i++) {
           if(  bancoOs[i][0].equals(""+cod)){ // FORÇA SER UMA STRING
                bancoOs[i][0] = "";
                bancoOs[i][1] = "";
            } 
        }
    }
    public void alterar() throws IOException{
        Scanner entrada = new Scanner(System.in);
        BufferedReader leitura = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("DIgite o código do que deverá alterar: ");
        int cod = entrada.nextInt();
        for (int i = 0; i < 3; i++) {
            if(bancoOs[i][0].equals(""+cod)){ // FORÇA SER UMA STRING
                System.out.print("Novo código: ");
                bancoOs[i][0] = ""+entrada.nextInt();
                System.out.print("Nova descrição: ");
                bancoOs[i][1] = ""+leitura.readLine();
            }
        }
    }
    public void listar(){
        for (int i = 0; i < 5; i++) {
                if(!bancoOs[i][0].equals("")){
                System.out.println("\nCódigo: "+bancoOs[i][0]+"\nDescrição: " +bancoOs[i][1]);
            }
        }
    }
}
