/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package control;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;
import model.cliente;

/**
 *
 * @author danesi
 */
public class controlCliente {
    cliente cli = new cliente();
    String[][] bancoCli = new String [8][8];
    public void iniciarBanco(){
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {
                bancoCli[i][j]="";
            }
        }
        System.out.println("Banco cliente iniciado!");
    }
    public int verificaArray(){
        int flag = 0;
        for (int i = 0; i < 8; i++) {
            if (bancoCli[i][0].equals("")) {
                flag++;
            }
        }
        flag=flag-8;
        if (flag<0)flag=flag*-1;
            return flag;
      
    }
    
    public void insere() throws IOException{
        Scanner entrada = new Scanner(System.in);
        BufferedReader leitura = new BufferedReader(new InputStreamReader(System.in));
        //iniciarBanco();
            System.out.print("Código: ");
            cli.setCodigo(leitura.readLine());
            System.out.print("Nome: ");
            cli.setNome(leitura.readLine());
            System.out.print("CPF: ");
            cli.setCPF(leitura.readLine());
            System.out.println("Endereço: ");
            cli.setEndereco(leitura.readLine());
            System.out.println("Telefone: ");
            cli.setTelefone(leitura.readLine());
        int ret=verificaArray();
        if(ret<5){
        bancoCli[ret][0] = ""+cli.getCodigo();
        bancoCli[ret][1] = ""+cli.getNome();
        bancoCli[ret][2] = ""+cli.getCPF();
        bancoCli[ret][3] = ""+cli.getEndereco();
        bancoCli[ret][4] = ""+cli.getTelefone();
        }
    }
    public void apagar(){
        Scanner entrada = new Scanner(System.in);
        System.out.print("Digite o código do que deverá apagar: ");
        int cod = entrada.nextInt();
        for (int i = 0; i < 4; i++) {
           if(  bancoCli[i][0].equals(""+cod)){ // FORÇA SER UMA STRING
                bancoCli[i][0] = "";
                bancoCli[i][1] = "";
                bancoCli[i][2] = "";
                bancoCli[i][3] = "";
                bancoCli[i][4] = "";
            } 
        }
    }
    public void alterar() throws IOException{
        Scanner entrada = new Scanner(System.in);
        BufferedReader leitura = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("DIgite o código do que deverá alterar: ");
        int cod = entrada.nextInt();
        for (int i = 0; i < 3; i++) {
            if(bancoCli[i][0].equals(""+cod)){ // FORÇA SER UMA STRING
                System.out.print("Novo código: ");
                bancoCli[i][0] = ""+entrada.nextInt();
                System.out.print("Nova Nome: ");
                bancoCli[i][1] = ""+leitura.readLine();
                System.out.print("Novo CPF: ");
                bancoCli[i][2] = ""+leitura.readLine();
                System.out.print("Novo Endereço: ");
                bancoCli[i][3] = ""+leitura.readLine();
                System.out.print("Novo Telefone: ");
                bancoCli[i][4] = ""+entrada.nextInt();
            }
        }
        
    }
    public void listar(){
        for (int i = 0; i < 4; i++) {
            //if(!bancoCli[i][0].equals("")){
                System.out.println("\nCódigo: "+bancoCli[i][0]+"\nNome: " +bancoCli[i][1]+"\nCPF: "+bancoCli[i][2]+"\nEndereço: "+bancoCli[i][3]+"\nTelefone: "+bancoCli[i][4]);
         // }
        }
    }
}
